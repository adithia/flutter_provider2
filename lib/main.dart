import 'package:flutter/material.dart';
import 'package:flutter_provider2/data/uiset.dart';
import 'package:flutter_provider2/pages/dashboard.dart';
import 'package:provider/provider.dart';

 void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(builder: (_) =>UiSet(),),
      ],
      child: MaterialApp(
        home: Dashboard(),
      ),
    );
  }
}
