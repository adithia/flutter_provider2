import 'package:flutter/material.dart';
import 'package:flutter_provider2/data/uiset.dart';
import 'package:flutter_provider2/settings/settings.dart';
import 'package:provider/provider.dart';


class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    
    var ui = Provider.of<UiSet>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Dashboard'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.settings),
            onPressed: (){
              Navigator.push(context, MaterialPageRoute(builder: (context)=> Settings()));
            },
          )
        ],
      ),
      body: Center(
        child: Text('Halow Selamat Datang', style: TextStyle(fontSize: ui.fontSize )),
      ),
    );
  }
}
