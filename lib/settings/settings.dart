import 'package:flutter/material.dart';
import 'package:flutter_provider2/data/uiset.dart';
import 'package:provider/provider.dart';


class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  Widget build(BuildContext context) {
    var ui = Provider.of<UiSet>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('settings'),
      ),
      body: Column(
        children: <Widget>[
          ListTile(
            title: Text('${ui.fontSize.toInt ()}'),
            subtitle: Slider(
              value: ui.sliderfontsize,
              min: 0.5,
              onChanged: (newValue){
                ui.fontSize = newValue;
              } ,),
          )
        ],)
      );
  }
}